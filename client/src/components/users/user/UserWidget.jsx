import React, { useEffect, useState, useContext } from 'react';
import { Box, Typography, Divider, useTheme, useMediaQuery, IconButton ,CircularProgress} from "@mui/material";
import { useSelector } from "react-redux";
import Dropzone from "react-dropzone";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import {
    ManageAccountsOutlined,
    EditOutlined,
    LocationOnOutlined,
    WorkOutlineOutlined,
    Twitter,
    LinkedIn,
    PersonAddOutlined,
    PersonRemoveOutlined,
 
  } from "@mui/icons-material";
import { useNavigate, useParams, useLocation } from "react-router-dom";
import UserAvatar from '../styledComponents/UserAvatar';
import FlexBetween from '../styledComponents/FlexBetween';
import WidgetWrapper from '../styledComponents/WidgetWrapper';
import PeopleIcon from '@mui/icons-material/People';
import UserWidgetSkeleton from './UserWidgetSkeleton';
import PersonIcon from '@mui/icons-material/Person';
import { AppContext } from '../../../utils/context';
import api from '../../../utils/api';
import { ChatUserRoute, FollowingRoute, setAvatarRoute} from '../../../utils/APIRoutes';
import { async } from 'regenerator-runtime';

function UserWidget({ username,userId, profilePhotoUrl }) {

  const { id } = useParams();
  const location = useLocation();
  const [user, setUser] = useState(null);
  const { palette } = useTheme();
  const navigate = useNavigate();
  const { dark, medium, main } = palette.neutral;
  const [loading, setLoading] = useState(false);
  const { light } = palette.primary;
  const [followingStatus, setFollowingStatus]  = useState('');
  const [errors, setErrors] = useState({});
  const [followerCount, setFollowerCount] = useState(0);
  const [followingCount, setFollowingCount] = useState(0);
  const isNonMobileScreens = useMediaQuery("(min-width: 800px)");
  const [imageUrls, setImageUrls] = useState([]);
  const [image, setImage] = useState(null);

  const { info} = useContext(AppContext);

  const toastOptions = {
    position: "bottom-right",
    autoClose: 8000,
    pauseOnHover: true,
    draggable: true,
    theme: "dark",
  };

    const  occupation =2 , viewedProfile = 2, impressions = 'tets', location1 = 'test';

  const  handleSearchMessage = async() =>{
    if(id){
      const res = await api.get(`${ChatUserRoute}/user/${id}`);
      if(res.status === 200){
        const data = res.data;
        navigate(`/chat/${data._id}`);
      }
      
      // console.log('false', res)

    }
  }

  const handleBlur = async () => {
    setErrors({});
  };

  const getFollowUser = async() =>{
    if(id && info){
      const res = await api.get(`${FollowingRoute}/${id}`);
      if(res.status === 200){
        if(res.data){
          const follow = res.data;
          if(info.userId === follow.creator){
            setFollowingStatus(follow.creatorStatus);
          } else{
            setFollowingStatus(follow.receiverStatus);
          }
        }
      }
    }
  }

  const getListFollowerUser = async() =>{
    if(id){
      const {data} = await api.get(`${FollowingRoute}/followers/${id}`);
      // console.log('aaa',res);
      setFollowerCount(data.length);
    } 
    else{
      const {data} = await api.get(`${FollowingRoute}/followers`);
      
      setFollowerCount(data.length);
    }
  }

  const getListFollowingUser = async() =>{
    if(id){
      const {data} = await api.get(`${FollowingRoute}/followings/${id}`);
      setFollowingCount(data.length);
    } else{
      const {data} = await api.get(`${FollowingRoute}/followings`);
      setFollowingCount(data.length);
    }
  }

  const handleDrop = async(acceptedFiles) => {
    acceptedFiles = acceptedFiles.slice(0, isNonMobileScreens ? 5 : 4);

    setImageUrls(
      acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        })
      )
    );
   
     const { data} = await api.post(`${setAvatarRoute}`,{file:acceptedFiles[0]});
     if (data.status === false) {
       toast.error(data.msg, toastOptions);
     }
     if (data.status === true) {
       localStorage.setItem('access_token', data.token.accessToken);
       localStorage.setItem('refresh_token', data.token.refreshToken)
       const accessToken = localStorage.getItem('access_token');
       if(accessToken){
         // console.log(accessToken);
         setTimeout(() => {  window.location.reload(); }, 50)
        //  navigate("/profile")
       }
       else {
         toast.error("Error setting avatar. Please try again.", toastOptions);
       }
     } 
  };



  const updateFollowing = async() =>{

    setLoading(true);
    const res = await api.post(`${FollowingRoute}`, {
      creator: info.userId,
      receiver: id,
      status: "Following",
    },
    {headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`,
    }}
    )

    if(res.status<400){
      const follow = res.data;
      if(info.userId === follow.creator){
        setFollowingStatus(follow.creatorStatus);
      } else{
        setFollowingStatus(follow.receiverStatus);
      }
      setLoading(false);
    }
  }
    useEffect(() => {
        getFollowUser();
        getListFollowerUser();
        getListFollowingUser();
    }, [id, info]);
  return (
    <WidgetWrapper>
      <FlexBetween gap="0.5rem" pb="1.1rem">
        <FlexBetween>
          <UserAvatar image={profilePhotoUrl} />
          {!id && location.pathname === '/profile' &&
          <Dropzone
          acceptedFiles=".jpg,.jpeg,.png"
          multiple={true}
          onDrop={handleDrop}
        >
          {({ getRootProps, getInputProps }) => (
           <Box {...getRootProps()} sx={{ position: 'relative' }} >
              <input onBlur={handleBlur} {...getInputProps()} />  
          <IconButton  sx={{
               alignItems: "center",
               position: 'absolute',
               marginLeft: '-15px',
               marginBottom: '0px',
               justifyContent: "center", backgroundColor: light, p: "0.4rem"}}>
          
          <EditOutlined sx={{ color: main, fontSize:'10px' }} />
          </IconButton>
          </Box>
          )}
          </Dropzone>
          }
          <Box marginLeft="1rem">
            <Typography
              variant="h4"
              color={dark}
              fontWeight="500"
              sx={{
                "&:hover": {
                  color: 'blueviolet',
                  cursor: "pointer",
                },
              }}
              onClick={() => navigate("/profile")}
            >
              {username}
            </Typography>
            <FlexBetween paddingTop="0.4rem" width="6rem">
              <FlexBetween>
                <Typography color={dark} marginRight="0.25rem">
                  {followerCount}
                </Typography>
                <Typography color={medium} fontSize='8px'>
                  {followerCount === 1 ? "follower" : "followers"}
                </Typography>
              </FlexBetween>

              { followingCount  && <FlexBetween>
                <Typography color={dark}>{followingCount}</Typography>
                <Typography color={medium} marginLeft="0.25rem" fontSize='8px'>
                  following
                </Typography>
              </FlexBetween>}
              
            </FlexBetween>
          </Box>
        </FlexBetween>

        { info && info.userId === userId || !id? (
           <ManageAccountsOutlined sx={{ marginLeft:'20px'}} />
        ) :(
          <Box  sx={{ display:'flex', flexDirection: "row",
          alignItems: "center",
          justifyContent: "center"}} >
          <Box sx={{
            width: "30px",
            height: "30px",
            alignItems: 'center',
            textAlign: 'center',
            mr:'0.4rem',
            "&:hover": {
              color: 'blueviolet',
              cursor: "pointer",
            },
          }}
          onClick={handleSearchMessage}
          >
          <img src="https://img.icons8.com/external-beshi-glyph-kerismaker/48/null/external-Messanger-communication-media-beshi-glyph-kerismaker.png"/>
          <Box fontSize='6px'>
          Message
          </Box>
          </Box>

          <Box sx={{ textAlign:'center', "&:hover": {
                  color: 'blueviolet',
                  cursor: "pointer",
                },}}>
          <IconButton
               onClick={() => updateFollowing()}
               disabled={loading}
               sx={{ display:'flex', flexDirection: "column",
               alignItems: "center",
               justifyContent: "center", backgroundColor: light, p: "0.4rem", mt:'0.5rem'}}
          >
             {loading ? (
          <CircularProgress size={20} />
        ) : followingStatus ? (
          <>
          {followingStatus === 'Following' && <PersonRemoveOutlined sx={{ color: dark, fontSize: '18px' }}  /> }
          {followingStatus === 'Friend' && <PeopleIcon sx={{ color: dark, fontSize: '18px' }}  />}
          {followingStatus === 'Follower' && <PersonIcon sx={{ color: dark, fontSize: '18px' }}  />}
          {followingStatus === '' && <PersonAddOutlined sx={{ color: dark, fontSize: '18px' }} />}
          </>
        ) : (
          <PersonAddOutlined sx={{ color: dark, fontSize: '18px' }} />
        )}
            </IconButton>
            <Box fontSize='6px'>
          {followingStatus}
          </Box>
          </Box>
          
          </Box>
        ) }
       
        
      </FlexBetween>

      <Divider />

      <Box p="1rem 0">
        <Box display="flex" alignItems="center" gap="1rem" mb="0.5rem">
          <LocationOnOutlined fontSize="large" sx={{ color: main }} />
          <Typography color={medium}>{location1}</Typography>
        </Box>
        <Box display="flex" alignItems="center" gap="1rem">
          <WorkOutlineOutlined fontSize="large" sx={{ color: main }} />
          <Typography color={medium}>{occupation}</Typography>
        </Box>
      </Box>

      <Box p="1rem 0">
        <FlexBetween mb="0.5rem">
          <Typography color={medium}>Who viewed your profile</Typography>
          <Typography color={main} fontWeight="500">
            {viewedProfile}
          </Typography>
        </FlexBetween>

        <FlexBetween>
          <Typography color={medium}>Impressions of your post</Typography>
          <Typography color={main} fontWeight="500">
            {impressions}
          </Typography>
        </FlexBetween>
      </Box>

      <Box p="1rem 0">
        <Typography color={main} marginBottom="0.7rem" fontWeight="500">
          Social Media Profiles
        </Typography>

        <FlexBetween gap="1rem" mb="0.5rem">
          <FlexBetween gap="1rem">
            <Twitter />
            <Box>
              <Typography color={main} fontWeight="500">
                Twitter
              </Typography>
              <Typography color={medium}>Social Media</Typography>
            </Box>
          </FlexBetween>
          <EditOutlined sx={{ color: main }} />
        </FlexBetween>

        <FlexBetween gap="1rem">
          <FlexBetween gap="1rem">
            <LinkedIn />
            <Box>
              <Typography color={main} fontWeight="500">
                Linkedin
              </Typography>
              <Typography color={medium}>Network</Typography>
            </Box>
          </FlexBetween>
          <EditOutlined sx={{ color: main }} />
        </FlexBetween>
      </Box>
    </WidgetWrapper>
  )
}

export default UserWidget